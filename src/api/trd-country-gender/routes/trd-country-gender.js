'use strict';

/**
 * trd-country-gender router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-country-gender.trd-country-gender');
