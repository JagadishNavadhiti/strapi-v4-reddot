'use strict';

/**
 * trd-country-gender service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-country-gender.trd-country-gender');
