'use strict';

/**
 *  trd-country-gender controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-country-gender.trd-country-gender');
