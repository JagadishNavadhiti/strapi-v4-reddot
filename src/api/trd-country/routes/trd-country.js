'use strict';

/**
 * trd-country router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-country.trd-country');
