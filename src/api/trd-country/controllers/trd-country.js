'use strict';

/**
 *  trd-country controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-country.trd-country');
