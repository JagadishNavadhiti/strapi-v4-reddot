'use strict';

/**
 * trd-country service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-country.trd-country');
