'use strict';

/**
 *  trd-language controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-language.trd-language');
