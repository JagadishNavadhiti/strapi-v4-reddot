'use strict';

/**
 * trd-language service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-language.trd-language');
