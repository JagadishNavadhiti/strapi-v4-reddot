'use strict';

/**
 *  trd-portal-main-content controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-portal-main-content.trd-portal-main-content');
