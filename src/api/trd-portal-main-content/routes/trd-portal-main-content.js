'use strict';

/**
 * trd-portal-main-content router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-portal-main-content.trd-portal-main-content');
