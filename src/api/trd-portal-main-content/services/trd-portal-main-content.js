'use strict';

/**
 * trd-portal-main-content service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-portal-main-content.trd-portal-main-content');
