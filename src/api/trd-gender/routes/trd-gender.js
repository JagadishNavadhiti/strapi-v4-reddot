'use strict';

/**
 * trd-gender router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-gender.trd-gender');
