'use strict';

/**
 *  trd-gender controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-gender.trd-gender');
