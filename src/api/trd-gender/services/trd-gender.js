'use strict';

/**
 * trd-gender service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-gender.trd-gender');
