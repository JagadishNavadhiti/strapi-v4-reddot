'use strict';

/**
 * trd-country-language service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-country-language.trd-country-language');
