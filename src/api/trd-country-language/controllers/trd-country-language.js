'use strict';

/**
 *  trd-country-language controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-country-language.trd-country-language');
