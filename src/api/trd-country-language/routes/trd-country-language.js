'use strict';

/**
 * trd-country-language router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-country-language.trd-country-language');
