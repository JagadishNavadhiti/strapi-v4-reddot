'use strict';

/**
 *  trd-portal-volunteer-request controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-portal-volunteer-request.trd-portal-volunteer-request');
