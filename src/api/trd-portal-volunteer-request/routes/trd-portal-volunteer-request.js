'use strict';

/**
 * trd-portal-volunteer-request router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-portal-volunteer-request.trd-portal-volunteer-request');
