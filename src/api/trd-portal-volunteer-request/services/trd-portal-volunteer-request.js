'use strict';

/**
 * trd-portal-volunteer-request service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-portal-volunteer-request.trd-portal-volunteer-request');
