'use strict';

/**
 * trd-portal-area-of-interest router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::trd-portal-area-of-interest.trd-portal-area-of-interest');
