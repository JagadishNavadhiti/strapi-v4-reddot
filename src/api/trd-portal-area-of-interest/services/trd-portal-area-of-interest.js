'use strict';

/**
 * trd-portal-area-of-interest service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::trd-portal-area-of-interest.trd-portal-area-of-interest');
