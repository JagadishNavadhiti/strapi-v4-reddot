'use strict';

/**
 *  trd-portal-area-of-interest controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::trd-portal-area-of-interest.trd-portal-area-of-interest');
