module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'd57c44e590ddbc5659607ea866c76695'),
  },
});
